package ch.mhf.div;

/**
 * @author Michael Friderich    //Klasse & Interface
 * @version 1.0                 //Klasse & Interface
 * @since						//Ueberall
 * @category CheatSheet
 * @param                       //Methoden & Konstruktoren
 * @throws                      //Methoden & Konstruktoren
 * @return                      //Methode
 * @see                         //Ueberall
 */

public class Comment
{
    public static void main(String[] args)
    {
        //Einzeiliger Kommentar

        /*
         * Mehrzeiliger
         * Kommentar
         */

        /**
         * Dokummentationskommentar
         * im Java Doc
         */
    }
}